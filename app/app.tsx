<div>
  // react/no-danger-with-children incorrect code
  <div>
    <div dangerouslySetInnerHTML={{ __html: "HTML" }}>
      Children
    </div>

    <Hello dangerouslySetInnerHTML={{ __html: "HTML" }}>
      Children
    </Hello>
  </div>

  // react/no-danger-with-children correct code
  <div>
    <div dangerouslySetInnerHTML={{ __html: "HTML" }} />

    <Hello dangerouslySetInnerHTML={{ __html: "HTML" }} />

    <div>
      Children
    </div>

    <Hello>
      Children
    </Hello>
  </div>
</div>
